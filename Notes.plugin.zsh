#!/usr/bin/zsh
# Notes
# A shell plugin to take Notes fast and easy directly from the commandline
# without lefting nasty files everywhere on your harddrive.
# It integrates in your workflow, so you just have to care for your savimng dir
  # once.
#  
# notes : Print a short help message
# addNote : You can add new notes with the addNote command
# editNote : you can edit the last taken note by editNote or passing the note you want to
# edit as the first parameter to editNote
# showNotes : prints a list of your taken notes while 
# catNote : prints out the last
# edited note with cat.
# rmNote : [the meant note name from the showNotes window] as parameter you can delete
# specific notes, while 
# cleareNotes : deleats all saved notes and brings you up to
# an empty desktop.
# searchNote : Search for a special hint from your younger me.
# resetNOTEPATH : let's you set your own destination to save your notes.
# 
# #############################################################################
#
# Distributed under GPL gnu public license v 2.0
# ##########################################
# 
# Some notes for installation
#
# Prerequisites:
# 
# The most of the needed commands should be automaticaly installed with yoour
# distribution.
# Those are:
# 
# cat
# mkdir
# mv
# echo
# read
# export
# 
# vim: Recomended : If you want to use another editor just replace every
  # acurrence of vimby your prefered editors launch command.
#  
#  Installation
#  
# Create a folder for this plugin in your .oh-my-zsh/custom/plugins folder named
  # Notes
mkdir -p PATH/TO/.oh-my-zsh/custom/plugins/Notes  
# Place this file into your /custom/plugins/Notes folder of the oh-my-zsh-installation
# and enable it by adding Notes to your plugins list as usual to use it.
# You can also just add the contents of this file to your bashrc or zshrc file.
# If you use this method, you might have to change the script internal path to
# your bashrc or zshrc, depending on which you are using.
# You can do this by executing
  # :%s/$HOME\/.zshrc/\/PATH\/TO\/YOUR\/.bashrc/g
  # and
  # :%s/.zshrc/YOURRCFILERC/g
# in vimcommand mode.  
# 
# ##################################################################################

# The standard location to save your notes.
STANDARDNOTEPATH=$HOME/Notes

# Print if the execution of the last command was successfull
print_SUCCESS() {
  if [ $? -eq 0 ]
  then
    echo "  success"
  else 
    echo "  fail"
  fi
}
# Adds a new note to your notes directory, if no directory is set it asks you
  # for your decission.
addNote() {
  DATE=$(date '+%C%y-%m-%d_%H-%M-%S_%N')
  VIMPATH=$(which vim)
  if [[ -d $NOTEPATH ]]
  then
    createNote $NOTEPATH $DATE
  else 
    print_help_NOTEPATH
    echo "Press enter to use the default location in " $HOME"/Notes. \n"
    printf "You can also set your new NOTEPATH here, a new directory notes \
      will be created. :" && read
    if [[ ${#REPLY} -gt 0 ]]
    then 
      cd $REPLY && mkdir -p Notes
      NOTEPATH=$REPLY/Notes
      resetNOTEPATH $NOTEPATH
      echo Your notes will be saved in "" $NOTEPATH
      createNote $NOTEPATH $DATE
    else 
      NOTEPATH=$HOME/Notes
      mkdir -p $NOTEPATH
      echo "export NOTEPATH="$NOTEPATH "\n" >> $HOME/.zshrc
      createNote $NOTEPATH $DATE
    fi
  fi
}

createNote() {
  NOTEPATH=$1
  DATE=$2
  FILEPATH=$NOTEPATH/$DATE
  touch $FILEPATH
  echo "#!"$VIMPATH >> $FILEPATH
  REPRTOUCH=$(cat $FILEPATH)
  vim $FILEPATH
  REPRVIM=$(cat $FILEPATH)
  deleteIfUnchanged $REPRTOUCH $REPRVIM
  if ! [[ $? -eq 0 ]]
  then
    addToIndex $NOTEPATH $DATE
  fi
}

# Ensures that you do not save unneeded empty notes.
deleteIfUnchanged() {
  REPRTOUCH=$1
  REPRVIM=$2
  if [[ $REPRTOUCH = $REPRVIM ]]
  then
    echo "deleteing"
    rm $NOTEPATH/$DATE
    print_SUCCESS
    return 0
  else 
    return 1
  fi
  
}

# Adds the current notes filename to the index of notes to be sure not to delete
# files that we have not created on our self.
addToIndex() {
  NOTEPATH=$1
  DATE=$2
  echo $DATE >> $NOTEPATH/.files.index
}

# Show all notes in your selected NOTEPATH
showNotes() {
  validateNOTEPATH
  if [[ $? -eq 0 ]]
  then
    ls -1 $NOTEPATH
  fi
}

# Checks if the selected NOTEPATH is in your trie and therefore can be used to
  # store your notes.
validateNOTEPATH() {
  if ! [ -d $NOTEPATH ]
  then
    print_help_NOTEPATH
    return 1
  fi
  
  return 0

}

# Shows the .file.index which stores all added notes.
showIndexOfNotes() {
  cat $NOTEPATH/.files.index
}

# Edits the note passed as parameter.
# If non is provimded it chooses the last edited note.
editNote() {
  if ! [[ -d $NOTEPATH ]]
  then
    print_help_NOTEPATH
    return 1
  fi
  
  if [[ ${#1} -gt 0 ]]
  then
    FILEPATH=$NOTEPATH/$1
  else
    FILENAME=$(getLastEditedNote)
    FILEPATH=$NOTEPATH/$FILENAME
  fi
  vim $FILEPATH
}

# Prints the filename of the last edited note.
getLastEditedNote() {
  if ! [ -d $NOTEPATH ]
  then
    print_help_NOTEPATH
    return 1
  fi
  
  BACK=$(pwd)
  cd $NOTEPATH
  FILENAME=$(ls -rcta1 | grep -v "\." | tail -n 1)
  cd $BACK
  echo $FILENAME
}

# Prints a note. You can specify the notes filename as parameter.
# Prints the last edited note if no parameter is given.
catNote() {
  if ! [ -d $NOTEPATH ]
  then
    print_help_NOTEPATH
    return 1
  fi
  
  if [[ ${#1} -gt 1 ]]
  then
    FILEPATH=$NOTEPATH/$1
  else
    FILENAME=$(getLastEditedNote)
    FILEPATH=$NOTEPATH/$FILENAME
  fi
  cat $FILEPATH
}

# Deletes a specific note. You can give its filename as parameter.
rmNote() {
  PARAM=$1
  if ! [ -d $NOTEPATH ]
  then
    print_help_NOTEPATH
    return 1
  fi
  
  if [[ $NOTEPATH = $HOME ]]
  then
    print_help_NOTEPATH
    return 1
  fi
  
  if [[ ${#PARAM} -lt 3 ]] || [[ $PARAM = " " ]] || [[ $PARAM = "-f" ]]
  then
    DELETE_LAST_EDITED  $PARAM
    print_SUCCESS
  else
    DELETE_SPECIFIC $1
  fi
}

# delete the last edited note
DELETE_LAST_EDITED() {
  if [[ $1 = "-f" ]]
  then
    REPLY="y"
  else
    printf "Do you realy want to delete the last note? [y/n]"  && read
  fi
  
  if [[ $REPLY = "y" ]] || [[ $REPLY = "Y" ]]
  then
    lastEditedNote=$(getLastEditedNote)
    BACK=$(pwd)
    cd $NOTEPATH
    PWD=$(pwd)
    if [[ $PWD = $NOTEPATH ]]
    then
      DELETE_NOTE $NOTEPATH $lastEditedNote
      cd $BACK
      return 0
    fi
    cd $BACK
    return 1
  fi
}

# delete a specific note provide the note name as $PARAM $1
DELETE_SPECIFIC() {
  BACK=$(pwd)
  cd $NOTEPATH && PWD=$(pwd)
  echo $PWD
#  PWD=$(pwd)

  if [[ $NOTEPATH = $PWD ]] 
  then
    DELETE_NOTE $NOTEPATH $1
    print_SUCCESS
  else
    echo $NOTEPATH
    echo "Sorry, NOTEPATH not found."
    print_help_NOTEPATH
    cd $BACK
    return 1
  fi
  cd $BACK
}

# Print the help message.
print_help_NOTEPATH() {
    echo "Plaese reset the location \"NOTEPATH\" to store your notes at."
    echo "You can do this by executing <resetNOTEPATH>, "
    echo "passing your chosen destination/path as the first argument $1."
}

# internal delete function.
DELETE_NOTE() {
  NOTEPATH=$1
  FILENAME=$2
  cat $NOTEPATH/.files.index | grep $FILENAME
  if [[ $? -eq 0 ]]
  then
    echo "rm " $NOTEPATH/$FILENAME
    rm $NOTEPATH/$FILENAME
    if [[ $? -eq 0 ]]
    then
      deleteFromIndex $FILENAME $NOTEPATH
    fi
  else 
    echo "This file is not any of your notes. It will therefore not be deleted."
    return 1
  fi
}

# Delete a FILENAME from the index of taken notes after deleting it with rmNotes
deleteFromIndex() {
  FILENAME=$1
  NOTEPATH=$2
  cat $NOTEPATH/.files.index | grep -v $FILENAME > $NOTEPATH/.files.temp.index
  cp $NOTEPATH/.files.index $NOTEPATH/.files.backup.index
  TEMP=$(cat $NOTEPATH/.files.temp.index)
  #echo "temp saved"
  #echo $TEMP
  cat $NOTEPATH/.files.backup.index | grep $TEMP >> /dev/null
  if [[ $? -eq 0 ]]
  then
    rm $NOTEPATH/.files.index
    mv $NOTEPATH/.files.temp.index $NOTEPATH/.files.index
    if [[ $? -eq 0 ]]
    then
      rm $NOTEPATH/.files.backup.index
    else 
      echo "Error : new INDEXFILE could not be moved to working name."
      echo "Please reindex your files."
      echo "You can do this with executing <reindexNotes>"
        return 1
    fi
  else 
    echo "Error : The new indexfile could not be saved to temp. Your index will not be changed."
    echo "In the most cases you can ignore this, but your INDEXFILE may becomme verry big if not deleting entrys while deleting notes from it."
      echo "To avoid this, you should reindex your notes by executing <reindexNotes>"
  fi
}

# Create a new INDEXFILE from the files stored in your NOTEPATH.
# As not using any regex yet all files will be added wheather they are created
# by the notes application ore not.
# So please use this command with caution.
reindexNotes() {
  echo "Attension! By reindexing your Notes all files in" $NOTEPATH "will be added to the index even if there are not created by the notes application. 
    They are not longer protected from beeing deleted by the rmNotes or the clearNotes commands. Are you sure you want to reindex? [y/n]" && read
  if [[ $REPLY = "y" ]] || [[ $REPLY = "Y" ]]
  then
    showNotes > $NOTEPATH/.files.index
    print_SUCCESS
    echo "Your new " $NOTEPATH"/.files.index "
    showIndexOfNotes
  fi
}

# Resets the NOTEPATH.
# THe NOTEPATH is the directory where all your notes are changed.
# Give it as parameter if you want to change it.
  # Otherwise the STANDARDNOTEPATH at $HOME/notes will be used.
resetNOTEPATH() {
  PARAMETER=$1
  if [[ ${#PARAMETER} -gt 1 ]] && [[ -d $PARAMETER ]]
  then
    NOTEPATH=$PARAMETER
    cat $HOME/.zshrc | sed 's/export NOTEPATH/#export NOTEPATH/g' > $HOME/.zshrc_NEW
    mv $HOME/.zshrc $HOME/.zshrc_BACKUP
    mv $HOME/.zshrc_NEW $HOME/.zshrc
    echo "export NOTEPATH="$NOTEPATH "\n" >> $HOME/.zshrc
    echo "Success : Your new NOTEPATH is now located in :"
    echo $NOTEPATH
    echo "Attension!"
    echo "Please be awhare that we use the rm command to delete notes."
    echo "Therefore do not place your NOTEPATH to a directory with important"
      echo "files in it."
      echo "We try to detecgt by deletion if the note you try to delete is yours "
        echo "but we can't garantee the this method will work in all cases."
      echo "So we recomend you to use an empty directory to store your Notes such as an newly created one. The default one is " $HOME"/Notes"
  else
    if [[ -d $STANDARDNOTEPATH ]]
    then
      NOTEPATH=$STANDARDNOTEPATH
      echo "NO valid NOTEPATH found by input, using standard"
      cat $HOME/.zshrc | sed 's/export NOTEPATH/#export NOTEPATH/g' > $HOME/.zshrc_NEW
      mv $HOME/.zshrc $HOME/.zshrc_BACKUP
      mv $HOME/.zshrc_NEW $HOME/.zshrc
      echo "export NOTEPATH="$STANDARDNOTEPATH "\n" >> $HOME/.zshrc
    else 
      echo "THe STANDARD NOTEPATH seams not to be a valid destination on this \
        machine. Please change it by provimding \
        \"export STANDARDNOTEPATH=A/VALID/PATH\" in your ~/.profile, \
        or bashrc / zshrc."
    fi
    echo $NOTEPATH
  fi
}

# Delete all your notes and bring you up with a new desktop
clearNotes() {
  if ! [ -d $NOTEPATH ]
  then
    print_help_NOTEPATH
    return 1
  fi
  
  if [[ $NOTEPATH = $HOME ]]
  then
    print_help_NOTEPATH
    return 1
  fi
  
  cd $NOTEPATH
  echo "Deleting all files in " $(pwd)
  printf "Is this OK? " "[y/n]" && read
  if [[ $REPLY = "y" ]] || [[ $REPLY = "Y" ]]
  then
    indexIsOpsoleed=1
    if [[ -f ./.files.index ]]
    then
      for i in $(ls -1)
      do
        cat ./.files.index | grep $i >> /dev/null
        if [[ $? -eq 0 ]]
        then
          rm $NOTEPATH/$i
          print_SUCCESS
          indexIsOpsoleed=0
        else 
          echo "Sorry : " $i " was not found in your index of Notes."
          echo "It seams not to be a part of your notebook."
        fi
      done
    else 
      echo "Error : index file not found."
      echo "Please check your NOTEPATH and reset it if it is lost."
      echo "Hint : Your NOTEPATH should point to an empty directory, fore example /home/USER/notes to avoid nasting files in your other dirs."
    fi
    
    if [[ $indexIsOpsoleed -eq 0 ]]
    then
      rm $NOTEPATH/.files.index
    fi
    
  else 
    echo "Canceled"
    return 1
  fi
}

# Print available commands.
notes() {
  echo "Notes V 1.0"
  echo "A small application to make notes on the commandline, fast, easy and without nasting your harddrive with unsorted data."
  echo "Usage : "
  echo "addNote : create a new Note with the current date and time as name in your NoteBookDestination."
  echo "showNotes : shows a list of your allready saved notes."
  echo "clearNotes : clears all notes and sets you up with a new frash NoteBook on your desktop."
  echo "searchNote : Searches your saved notes for a given STRING. You can"
    echo "pass your SEARCHTERM as the first parameter $1, or execute the "
    echo "searchNote command without parameters in interactive mode."
  echo "editNote : edits your last created note, you can pass every filename from the showNotes window to edit a specific note."
  echo "catNote : Shows your last created note with cat. You can pass any filename from the showNotes window to show a specific note."
  echo "rmNote : Deletes the Note with the as parameter $1 specified filename. If non is given the last edited note is deleted after confirmation. "
  echo "resetNOTEPATH : Resets the NoteBooks savimng destination, you can pass any path as parameter $1."
  echo "clearSwapedNotes  :  clear your NOTEPATH from all swapped notefiles. Attension! No indexing used"
  echo "catUnchecked  :  cat all lines from all notes that are not marked as checked. You can check a line by writing (check) or (checked) anywhere on the line "
  echo "If you provide no parameter the merge will be deleted after showing to stdout to avoid doubling lines. If you want to save it provide -s as parameter $1 "
  echo "You can filter your output after headings "
  echo "TO mark a line as heading place a # anywhere on the line "
  echo "TO use multiple heading levels use multiple # signs for example ## for heading level 2"
    echo "Use catUnchecked -h [heading-level-as number] to filter for a specific heading level"
      echo "valid numbers are 1 - 6"
      echo "For example "
      echo "catChecked -h 1  :  list all unchecked notes at heading level 1"
  echo "showUncheckedNotes  :  same as catUnchecked "
}

# Alias for notes
note() {
  notes
}

# Alias for notes
NOTE() {
  notes
}

# Alias for notes
NOTES() {
  notes
}

# Search for a special note.
  # You can launch it with the SEARCHTERM as the parameter $1 or without
  # parameters in interactive mode.
searchNote() {
  PARAMETER_SEARCHTERM=$1
  if [[ ${#PARAMETER_SEARCHTERM} -lt 1 ]]
    then
      while true
      do
        echo "What are you searching for? "
        read
        SEARCHTERM=$REPLY
        grep -Ril $SEARCHTERM $NOTEPATH
        if [[ $? -eq 0 ]]
        then
          cat $(grep -Ril $SEARCHTERM $NOTEPATH)
      else 
        echo "No results found."
      fi
      done
    elif [[ ${#PARAMETER_SEARCHTERM} -ge 1 ]]
    then
      SEARCHTERM=$1
      grep -Ril $SEARCHTERM $NOTEPATH
      if [[ $? -eq 0 ]]
      then
        cat $(grep -Ril $SEARCHTERM $NOTEPATH)
      else 
        echo "No results found."
      fi
    fi
}

# Clear NOTEPATH from swap files
clearSwapedNotes() {
  echo ""Attension! 
  printf "This will delete all swapfiles in your NOTEPATH. You will not be able to get them back. Are you sure to clear? [y/n] "  && read
  if [[ $REPLY = "y" ]] || [[ $REPLY = "Y" ]]
  then
    rm $NOTEPATH/.*.swp
    rm $NOTEPATH/.*.swo
  fi
  print_SUCCESS
}

# Merge all unchecked notes together in one NoteFile
mergeUncheckedNotes() {
  addEmptyNote
  if [[ $? -eq 1 ]]
  then
    return 1
  fi
  
  NEWNOTE=$(getLastEditedNote)
  NEWFILEPATH=$NOTEPATH/$NEWNOTE
  #echo "New Note " $NEWNOTE
  for i in $(showNotes)
  do
    #echo $i
    if ! [[ $i = $NEWNOTE ]]
    then
      cat $NOTEPATH/$i | grep -v "(check" | grep -v "((checked)" |grep -v "#!" >> $NEWFILEPATH
    fi
  done
  
}

addEmptyNote() {
  DATE=$(date '+%C%y-%m-%d_%H-%M-%S_%N')
  if [[ -d $NOTEPATH ]]
  then
    touch $NOTEPATH/$DATE
    addToIndex $NOTEPATH $DATE
    return 0
  else
    return 1
  fi
}

# cat all unchecked notes
catUnchecked() {
  mergeUncheckedNotes
  filter_headings $1 $2
  if [[ ${#1} -lt 1 ]] || ! [[ $1 = "-s" ]]
  then
    rmNote -f >> /dev/null
  elif [[ $1 = "-s" ]]
  then
    echo "Keeping merge as new note "
  fi
}

filter_headings() {
  if [[ $2 = "1" ]]
  then
    HEADING="\#\#"
  elif [[ $2 = "2" ]]
  then
    HEADING="\#\#\#"
  elif [[ $2 = "3" ]]
  then
    HEADING="\#\#\#\#"
  elif [[ $2 = "4" ]]
  then
    HEADING="\#\#\#\#\#"
  elif [[ $2 = "5" ]]
  then
    HEADING="\#\#\#\#\#\#"
  elif [[ $2 = "6" ]]
  then
    HEADING="\#\#\#\#\#\#\#"
  fi

  if [[ $1 = "-h" ]]
  then
    #echo $HEADING
    catNote | grep -wv $HEADING | grep -v "(checked)" | grep -v "(check)" | grep -v "(Checked)" | grep -v "(Check)"
  elif [[ $1 = "-H" ]]
  then
    HEADING=$2
    catNote | grep -v $HEADING | grep -v "(checked)" | grep -v "(check)" | grep -v "(Checked)" | grep -v "(Check)"
  else 
    catNote | grep -v "(check)" | grep -v "(checked)" | grep -v "(Check)" | grep -v "(Checked)"
  fi
}


# Alias for catUnchecked 
showUncheckedNotes() {
  catUnchecked $1 $2
}

